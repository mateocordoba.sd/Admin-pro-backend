const express = require('express');
const cors = require('cors');

// este require indica a dotenv que defina 
//como variables de entorno 
//las variables encontradas en el archivo .env
require('dotenv').config(); 

const { dbConnection } = require('./db/config');


const app = express();

/**
 * MIDLEWARES
 */

app.use( cors() );
app.use( express.json() ); // Parséo del body


dbConnection();

/**
 * RUTAS
 */



// Carpeta pública
app.use(express.static('public'));



// Log-In
app.use('/api/login', require('./routes/login'));

// usuarios
app.use('/api/usuarios', require('./routes/usuarios'));

// Hospitales
app.use('/api/hospitales', require('./routes/hospitales'));

// Médicos
app.use('/api/medicos', require('./routes/medicos'));

// Busqueda
app.use('/api/buscar', require('./routes/busquedas'));

// Upload Files
app.use('/api/uploads', require('./routes/uploads'));





app.get("/", (req, res)=> {

    res
    .status(200)
    .json({
        ok:true,
        msg:"Hola mundo"
    })

});




app.listen(process.env.PORT, () => {
    console.log("Servidor corriendo exitosamente en en puerto http://localhost:4201");
});