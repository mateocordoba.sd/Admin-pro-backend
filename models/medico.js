const {Schema, model} = require('mongoose');


const MedicoSchema = Schema({
    
         nombre:{
            type:'String',
            required:true,

         },      
         img:{
            type:'String',

         },
         email:{
            type:'String',
            default:''

         },
         usuario:{
            type: Schema.Types.ObjectId,
            ref:'Usuario',
            required:true
         },

         hospital:{
            type: Schema.Types.ObjectId,
            ref:'Hospital',
            required:true
         }

        
},{collection:"medicos"});

/**
 * Editamos la función por defecto proveniente de la clase UsuarioSchema
 */
MedicoSchema.method('toJSON', function (){
   // Destructuramos el objeto que recibimos para tomar solo los datos necesarios
   const {__v,_id, ...object} = this.toObject();
   object.id = _id
   return object
})
module.exports = model('Medico',MedicoSchema)


