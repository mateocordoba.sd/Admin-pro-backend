const {Schema, model} = require('mongoose');

const UsuarioSchema = Schema({
    
         nombre:{
            type:'String',
            required:true,

         },
        
         email:{
            type:'String',
            required:true,
            unique:true
         },
        
         password:{
            type:'String',
            required:true
         },
         role:{
            type:'String',
            required:true,
            default:"USER_ROLE"
            
         },
         google:{
            type:'String',
            default:false
         },
         img:{
            type:'String',

         },
         token:{
            type:'String'
         }
        
});

/**
 * Editamos la función por defecto proveniente de la clase UsuarioSchema
 */
UsuarioSchema.method('toJSON', function (){
   // Destructuramos el objeto que recibimos para tomar solo los datos necesarios
   const {__v,_id, password, ...object} = this.toObject();
   object.id = _id
   return object
})
module.exports = model('Usuario',UsuarioSchema)

