const {Schema, model} = require('mongoose');

const HospitalSchema = Schema({
    
         nombre:{
            type:'String',
            required:true,

         },        
         img:{
            type:'String',
         },
         
         usuario: {
            type:Schema.Types.ObjectId,
            ref:"Usuario"
        }
       
        
},{collection: 'hospitales'});

/**
 * Editamos la función por defecto proveniente de la clase UsuarioSchema
 */
HospitalSchema.method('toJSON', function (){
   // Destructuramos el objeto que recibimos para tomar solo los datos necesarios
   const {__v,_id,...object} = this.toObject();
   object.id = _id
   return object
})
module.exports = model('Hospital',HospitalSchema)

