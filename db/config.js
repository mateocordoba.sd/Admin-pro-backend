const mongoose = require('mongoose');
mongoose.set('strictQuery', true);
const { MongoClient, ServerApiVersion } = require('mongodb');

const dbConnection = async ()=>{
    const url = process.env.DB_URL;

    //console.log("Conectando con mongoose");

    // Cuando se presenta el error de conexión con moggose se debe utilizar el connection-string otrogada por la versión  "2.2.12 ot later"
    mongoose.connect(url,{ useNewUrlParser: true, useUnifiedTopology: true })
    
    console.log("DB CONNECTED...");
    //console.log("ERROR",error);
    

    /**
     * Conexión con con el cliente directo de 
     * 
    const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
    const collection = await client.db("HOSPITAL_DB").collection('usuarios').count();
    console.log( collection);
    */
        //client.close();
       
        
    
    
} 
module.exports = {
    dbConnection
}