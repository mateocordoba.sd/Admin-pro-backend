
const Medico = require('../models/medico');



const getMedicos = async (req, res) => {

    const medicos = await Medico.find({}, "nombre")
                                .populate('usuario','nombre img')
                                .populate('hospital','nombre img');
    console.log(medicos);




    res
        .status(200)
        .json({
            ok: true,
            medicos: medicos,
        })

}

const postMedico = async (req, res) => {

    console.log("POST Medico");

  
    try {
        

        const { nombre } = req.body;

        console.log(nombre);

        // Verificamos si ya existe un médico con las mismas credenciales
        const MedicoDB = await Medico.findOne({ nombre })

        if (MedicoDB) {

            console.log(MedicoDB, 'existe');
            return res
                .status(500)
                .json({
                    ok: false,
                    msg: "Ya existe un médico con el mismo nombre",
                })

        }

        



        const uId = req.uid


        const medico = new Medico({
            usuario:uId,
            ...req.body
        });

        console.log("MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",medico);
   
        await medico.save();

        console.log({medico});



        res
            .status(200)
            .json({
                ok: true,
                msg: "Creando Medico",
                medico,
            })
        

    } catch (error) {

        res
            .status(500)
            .json({
                ok: false,
                msg: error,
            })

    }


}

const putMedico = async (req, res) =>{

    const id = req.params.id;

    
    try {

        const existeMedico = await Medico.findById(id);
        console.log(existeMedico);

        
        if(!existeMedico){
            return res
            .status(404)
            .json({
                ok:false,
                msg:"No se encuentra Medico con ese id"
            })
            
        }

        const {...campos} = req.body;
       
        const updatedMedico = await Medico.findByIdAndUpdate(id, campos,{new:true}); 
        console.log(updatedMedico);

        res
            .status(200)
            .json({
                ok:true,
                msg:"El Medico se ha editado con éxito",
                id:id,
                updatedMedico
            })

        
    } catch (error) {


        if(error.codeName === "DuplicateKey"){

            if(error.keyPattern.email){

                return res
                .status(400)
                .json({
                    ok:false,
                    msg:'Ya existe un Medico con el E-mail'+ error.keyValue.email
    
                })
            }
        }


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })
        
    }

}


const deleteMedico = async (req, res) =>{
    const id = req.params.id;

    try {

        const delRes = await Medico.findByIdAndDelete(id);

        console.log(delRes);

        if (!delRes) {
            return res
            .status(404)
            .json({
                ok:false,
                msg:"El Medico que intentas eliminar, no exista o ya fue eliminado"
            })
            
        }


        res
            .status(200)
            .json({
                ok:true,
                msg:"El Medico se ha eliminado con éxito",
            })

        
    } catch (error) {


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })
        
    }
}




module.exports = {
    getMedicos,
    postMedico,
    putMedico,
    deleteMedico
}