const path =  require('path')

const { response } = require('express')
const Hospital = require("../models/hospital");
const Medico = require("../models/medico");
const Usuario = require("../models/usuario");

const fs = require('fs')



const { v4: uuidv4 } = require('uuid');
const { actualizarImg } = require("../helpers/actualizar-imagen");



const uploadImg = async (req, res) => {

    const tipo = req.params.tipo;
    const uId  = req.uid;
    const id   = req.params.id;
   

    const tiposValido = ["usuarios", "medicos", "hospitales"]

    if(!tiposValido.includes(tipo)){
        return res
                .status(400)
                .json({
                    ok:false,
                    msg:"El tipo enviado no es usuario, médico u hospital"
                })
    }

    if(!req.files || Object.keys(req.files).length === 0){

        return res
                .status(400)
                .json({
                    ok:false,
                    msg:"No se enviaron archivos"
                })

    }


    // Procesamos la imagen

    const file = req.files.imagen;


    const nombreCortado = file.name.split('.');

    const extension = nombreCortado[nombreCortado.length -1];

    const extensionesValidads = ['png','jpg','jpeg','gif'];


    if(!extensionesValidads.includes(extension)){
        return res
                .status(403)
                .json({
                    ok:false,
                    msg:"Tipo de imagen no permitido  solo se permiten 'png','jpg','jpeg','gif' "
                })
    };


    // Genrramos el nombre de la imagen

    const nombreArchivo = `${uuidv4()}.${extension}`;


    //Path para guardar la imagen 

    const path = `uploads/${tipo}/${nombreArchivo}`;


    file.mv(path,(err)=>{
        if(err){
            return res
                .status(500)
                .json({
                    ok:false,
                    msg:"Error al mover la imaagen al directorio de destino"
                })

        }

        //Actualizar la base de datos
        actualizarImg(tipo, id, path, nombreArchivo)


        res
        .status(200)
        .json({
            ok: true,
            id,
            tipo,
            nombreArchivo

        })


    })

    



}


const getImagen = (req, res = response) => {

    const tipo = req.params.tipo;
    
    const fotoId = req.params.fotoId;

    const pathImg =  path.join(__dirname,`../uploads/${tipo}/${fotoId}`);
    
    if(fs.existsSync(pathImg)){
        
        // El método sendFile nos retorna un archivo en la response de nuestra petición
        res.sendFile(pathImg);
        
    }else{

        console.log("NO SE ENCONTRO IMAGEN")
        
        // imagen por defecto
        const pathImg =  path.join(__dirname,`../uploads/no-img.jpg`);

        res.sendFile(pathImg);
        

    }


    //console.log({pathImg})




    
}


module.exports = {
    uploadImg,
    getImagen

}