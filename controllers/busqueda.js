const Hospital = require("../models/hospital");
const Medico = require("../models/medico");
const Usuario = require("../models/usuario");

const getTodo = async (req, res) => {

    const busqueda = req.params.busqueda;

    console.log(busqueda);

    try {
        // Usamos una expresión regular para controla que la búsqueda no tenga que concordar exactamente
        // Usamos en la experesión regular la bandera 'i' para indica que nuestra búsqueda sea insensible(que no tenga que ser una concordancia exacta no en la cadena de texto ni en en el case de la palabra(mayúscula o minúsculas))
        const regex = new RegExp(busqueda, 'i');

        const [usuarios, medicos, hospitales] = await Promise.all([

            Usuario.find({ nombre: regex }),
            Medico.find({ nombre: regex }),
            Hospital.find({ nombre: regex }),
        ])

        res.status(200)
            .json({
                ok: true,
                usuarios,
                medicos,
                hospitales
            })

    } catch (error) {
        res.status(500)
            .json({
                ok: false,
                error,
                msg: "ha habido un error comuníquese con el administrador"
            })

    }




}


const getDatosColeccion = async (req, res) => {


    const busqueda = req.params.busqueda;

    const tabla = req.params.tabla;


    // Usamos la bandera 'i' ára definirle a nuestro back-end que, la búsqueda sea case insensitive
    const regex = new RegExp(busqueda, 'i');

    let data = [];

    switch (tabla) {
        case 'usuarios':
            console.log("buscando usuarios");
            data = await Usuario.find({ nombre: regex });


            break;

        case 'medicos':
            data = await Medico.find({ nombre: regex })
                .populate('usuario', 'nombre img')
                .populate('hospital', 'nombre img');


            break;

        case 'hospitales':
            data = await Hospital.find({ nombre: regex })
                .populate('usuario', 'nombre img')


            break;



        default:

            return res
                .status(500)
                .json({
                    ok: false,
                    msg: "Nombre de colección inválido, debe ser 'usuarios', 'hospitales' 'medicos'"
                })


            break;

    }
    res
        .status(200)
        .json({
            ok: true,
            data
        })

    





}

module.exports = {
    getTodo,
    getDatosColeccion
}