
const Hospital = require('../models/hospital');




const getHospitals = async (req, res) => {

    const hospitals = await Hospital.find({}, "nombre")
                                    .populate('usuario','nombre img');
    console.log(hospitals);




    res
        .status(200)
        .json({
            ok: true,
            Hospitals: hospitals,
        })

}

const postHospital = async (req, res) => {

    console.log("POST HOSPITAL");

  
    try {
        

        const { nombre, img } = req.body;

        const HospitalDB = await Hospital.findOne({ nombre })

        if (HospitalDB) {

            console.log(HospitalDB, 'existe');
            return res
                .status(500)
                .json({
                    ok: false,
                    msg: "Ya existe un hospital con el mismo e-mail",
                })

        }

        const uId =  req.uid;

        const hospital = new Hospital({usuario:uId, ...req.body});

        console.log("CREANDO Hospital");

        await hospital.save();

        console.log({hospital});



        res
            .status(200)
            .json({
                ok: true,
                msg: "Creando Hospital",
                hospital,
            })
        

    } catch (error) {

        res
            .status(500)
            .json({
                ok: false,
                error,
                msg: "Hable con el administrador"
            })

    }


}

const putHospital = async (req, res) =>{

    const id = req.params.id;

    
    try {

        const existeHospital = await Hospital.findById(id);
        console.log(existeHospital);

        
        if(!existeHospital){
            return res
            .status(404)
            .json({
                ok:false,
                msg:"No se encuentra Hospital con ese id"
            })
            
        }

        const {...campos} = req.body;

       
        const updatedHospital = await Hospital.findByIdAndUpdate(id, campos,{new:true}); 
        console.log(updatedHospital);

    
        

        res
            .status(200)
            .json({
                ok:true,
                msg:"El Hospital se ha editado con éxito",
                id:id
            })

        
    } catch (error) {


        if(error.codeName === "DuplicateKey"){

            if(error.keyPattern.email){

                return res
                .status(400)
                .json({
                    ok:false,
                    msg:'Ya existe un Hospital con el E-mail'+ error.keyValue.email
    
                })
            }
        }


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })
        
    }

}


const deleteHospital = async (req, res) =>{
    const id = req.params.id;

    try {

        const delRes = await Hospital.findByIdAndDelete(id);

        console.log(delRes);

        if (!delRes) {
            return res
            .status(404)
            .json({
                ok:false,
                msg:"El Hospital que intentas eliminar, no exista o ya fue eliminado"
            })
            
        }


        res
            .status(200)
            .json({
                ok:true,
                msg:"El Hospital se ha eliminado con éxito",
            })

        
    } catch (error) {


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })
        
    }
}




module.exports = {
    getHospitals,
    postHospital,
    putHospital,
    deleteHospital
}