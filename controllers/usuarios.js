
const bcrypt =  require ('bcryptjs')
const Usuario = require('../models/usuario');

const {generarToken} = require('../helpers/jwt');



const getUsuarios = async (req, res) => {

    const desde =  req.query.desde || 0;
    console.log({desde});

    const [users, total] = await Promise.all([
        Usuario.find({}, "role nombre email google img")
        .skip(desde)
        .limit(5),
        
        Usuario.count()
    ])

    console.log(users);
    console.log(total);


    res
        .status(200)
        .json({
            ok: true,
            usuarios: users,
            uid: req.uid,
            total
        })

}

const postUsuario = async (req, res) => {

  
    try {
        

        const { password, email } = req.body;

        const usuarioDB = await Usuario.findOne({ email })

        if (usuarioDB) {

            console.log(usuarioDB, 'existe');
            return res
                .status(500)
                .json({
                    ok: false,
                    msg: "Ya existe un suario con el mismo e-mail",
                })

        }

        const usuario = new Usuario(req.body);

        // // Encriptamos la contraseña del usuario
        const salt = bcrypt.genSaltSync();



        usuario.password = bcrypt.hashSync(password,salt);
        console.log("CREANDO USUARIO");

        const token = await generarToken(usuario.id);
        console.log(`TOKEN: ${token}`);

        if (!token) {

            return res
            .status(500)
            .json({
                ok:false,
                msg:`No se pudo generar el token`
            })

            
        }

        usuario.token = token;



        await usuario.save();

        console.log({usuario});



        res
            .status(200)
            .json({
                ok: true,
                msg: "Creando Usuario",
                usuario,
                token
            })
        

    } catch (error) {

        res
            .status(500)
            .json({
                ok: false,
                msg: error,
            })

    }


}

const putUsuario = async (req, res) =>{

    const id = req.params.id;

    
    try {

        const existeUsuario = await Usuario.findById(id);
        console.log(existeUsuario);

        
        if(!existeUsuario){
            return res
            .status(404)
            .json({
                ok:false,
                msg:"No se encuentra usuario con ese id"
            })
            
        }

        
        const {password,google, email,...campos} = req.body;
        if(existeUsuario.email !== email){
            const existeEmail =  await Usuario.findOne({email});
            if(existeEmail){
                return res
            .status(404)
            .json({
                ok:false,
                msg:`Ya existe un usuario con el E-mail  ${email}`
            })

            }
        }

        if (!existeUsuario.google) {
            
            campos.email = email;
        }else if(existeUsuario.email !== email){
            return res
            .status(404)
            .json({
                ok:false,
                msg:`Usuario de google no pueden editar el E-mail`
            })

            

        }

        //const usuario = new Usuario(campos);

       
        const updatedUser = await Usuario.findByIdAndUpdate(id, campos,{new:true}); 
        console.log(updatedUser);

    
        

        res
            .status(200)
            .json({
                ok:true,
                msg:"El Usuario se ha editado con éxito",
                id:id
            })

        
    } catch (error) {


        if(error.codeName === "DuplicateKey"){

            if(error.keyPattern.email){

                return res
                .status(400)
                .json({
                    ok:false,
                    msg:'Ya existe un usuario con el E-mail '+ error.keyValue.email
    
                })
            }
        }


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })
        
    }

}


const deleteUsuario = async (req, res) =>{
    const id = req.params.id;

    try {

        const delRes = await Usuario.findByIdAndDelete(id);

        console.log(delRes);

        if (!delRes) {
            return res
            .status(404)
            .json({
                ok:false,
                msg:"El usuario que intentas eliminar, no exista o ya fue eliminado"
            })
            
        }


        res
            .status(200)
            .json({
                ok:true,
                msg:"El Usuario se ha eliminado con éxito",
            })

        
    } catch (error) {


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })
        
    }
}




module.exports = {
    getUsuarios,
    postUsuario,
    putUsuario,
    deleteUsuario
}