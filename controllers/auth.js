const bcrypt = require('bcryptjs');

const { response } = require('express')

const { googleVerify } = require('../helpers/google-verify');

const {generarToken} = require('../helpers/jwt')

const Usuario = require('../models/usuario');






const login = async (req, res) => {

    const {email, password} = req.body;



    try{




        const usuarioDB = await Usuario.findOne({email});

        console.log({usuarioDB});

        if(!usuarioDB){

            return res
            .status(404)
            .json({
                ok:false,
                msg:`No se encuentra usuario con el email: ${ email }`
            })

        }

        const validarPassword = bcrypt.compareSync(password, usuarioDB.password);



        if(!validarPassword){

            return res
            .status(400)
            .json({
                ok:false,
                msg:`Contraseña incorrecta`
            })

        }

        const token = await generarToken(usuarioDB.id)

        if (!token) {

            return res
            .status(500)
            .json({
                ok:false,
                msg:`No se pudo generar el token`
            })

            
        }

        console.log(`TOKEN: ${token}`);


        res
        .status(200)
        .json({
            ok:true,
            msg:`Inicio de sesión exitoso`,
            usuario:usuarioDB,
            token:token
        })

    }catch(error){


        res
            .status(500)
            .json({
                ok:false,
                msg:error

            })



    }



}



const loginGoogle = async (req, res = response) => {

    
    try {
        const {name, email, picture} = await googleVerify(req.body.token);

        const usuarioDB = await Usuario.findOne({ email });

        let usuario;

        if (!usuarioDB) {

            usuario = new Usuario({
                nombre:name,
                email,
                password: '@@@',
                img:picture,
                google:true

            })           

            
        }else{

            usuario = usuarioDB;
            usuario.google = true;

        }

        await usuario.save();


        const token = await generarToken(usuario.id)

        if (!token) {

            return res
            .status(500)
            .json({
                ok:false,
                msg:`No se pudo generar el token`
            })

            
        }




        
        res
        .status(200)
        .json({
            ok:true,
            usuario,
            token
    
        })
    } catch (error) {
        console.log(error);

        return res
                .status(500)
                .json({
                    ok:false,
                    msg:"Habale con el administrador"


                })
        
    }



}


const renewToken = async(req, res) => {

    const uid = req.uid;

    const usuario = await Usuario.findById(uid)

    const {password, ...datos } = usuario._doc;

    console.log({password}, {datos}, {usuario} );


    const token =  await generarToken(uid);

    res
        .json({
            ok:true,
            usuario:datos,
            token
        })



}



module.exports = {
    login,
    loginGoogle,
    renewToken
}