const Hospital = require("../models/hospital");
const Medico = require("../models/medico");
const Usuario = require("../models/usuario");


// fs nos permite trabajar con archivos
const fs = require('fs');

const borrarImagen = (path) => {

    if (fs.existsSync(path)) {
        fs.unlinkSync(path)
    }
    


}

const actualizarImg = async (tipo, id, path, nombreArchivo) => {


    let pathViejo = "";

    switch (tipo) {

        case 'usuarios':

            const usuario = await Usuario.findById(id);

            if (!usuario) {
                console.log("No se encuentra usuario con el id enviado");

                return false
            }
                    
            // buscamos si ya el usuario tiene una imagene guardada 
            pathViejo = `./uploads/usuarios/${usuario.img}`;

            
            borrarImagen(pathViejo)



            usuario.img = nombreArchivo;
            await usuario.save();


            return true




            break;
        case 'medicos':

            const medico = await Medico.findById(id);

            if (!medico) {
                console.log("No se encuentra Médico con el id enviado");

                return false
            }

            console.log(medico);

            pathViejo = `./uploads/medicos/${medico.img}`;

            borrarImagen(pathViejo)

            medico.img = nombreArchivo;
            await medico.save();
            return true


            break;

        case 'hospitales':

            const hospital = await Hospital.findById(id);

            if (!hospital) {
                console.log("No se encuentra hospital con el id enviado");

                return false
            }

            pathViejo = `./uploads/hospitales/${hospital.img}`;

            borrarImagen(pathViejo)

            hospital.img = nombreArchivo;
            await hospital.save()

            return true




            break;

        
    }

}



module.exports = {
    actualizarImg
}