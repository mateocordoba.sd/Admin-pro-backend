/**
 * Ruta: api/uploads/
 */
const {Router} = require('express')
const router = Router()

const {validarToken} = require('../middlewares/validarToken')

const {uploadImg, getImagen} =  require('../controllers/uploads')

const expressFileUpload =  require('express-fileupload')


router.use(expressFileUpload());
router.put(
    '/:tipo/:id',
    [
     validarToken           
    ],
    uploadImg

);



router.get(
    '/:tipo/:fotoId',
    // [
    //  validarToken           
    // ],
    getImagen

)



module.exports = router;