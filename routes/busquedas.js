/**
 * Ruta: api/buscar/
 */


const {Router} = require('express')

const router = Router()

const {validarToken} = require('../middlewares/validarToken')

const {getTodo,getDatosColeccion} =  require('../controllers/busqueda')



router.get(
    '/todo/:busqueda',
    [
     validarToken           
    ],
    getTodo

)

router.get(
    '/coleccion/:tabla/:busqueda',
    [
     validarToken           
    ],
    getDatosColeccion

)


module.exports = router;