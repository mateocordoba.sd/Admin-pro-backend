const {Router} = require('express');
const { check } = require('express-validator');
const router = Router();


const { getHospitals, postHospital,putHospital, deleteHospital } = require('../controllers/hospitales');
const { validarCampos } = require('../middlewares/validarCampos');
const { validarToken } = require('../middlewares/validarToken');




router.post(
    `/`,
   [
    validarToken,
    check('nombre','El nombre es obligatorio').not().isEmpty(),// El middleware "Check", nos permite Validar si un campo, ya declarado en el modelo, cimple con la validación declarada ene l mismo middleware en este caso .not().isEmpty()
    validarCampos
    ],
    postHospital
)

router.get(
    `/`,
    [
        validarToken
    ],
    getHospitals 
)


router.put(
    '/:id',
    [
        validarToken,
        check('nombre','El nombre es obligatorio').not().isEmpty(),// El middleware "Check", nos permite Validar si un campo, ya declarado en el modelo, cimple con la validación declarada ene l mismo middleware en este caso .not().isEmpty()
        validarCampos,

    ],

    putHospital
)

router.delete(
    '/:id',
    [
        validarToken
    ],
    deleteHospital
)






module.exports = router