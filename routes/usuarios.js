const {Router} = require('express');
const { check } = require('express-validator');
const router = Router();


const { getUsuarios, postUsuario,putUsuario, deleteUsuario } = require('../controllers/usuarios');
const { validarCampos } = require('../middlewares/validarCampos');
const { validarToken } = require('../middlewares/validarToken');




router.post(
    `/`,
   [
    check('nombre','El nombre es obligatorio').not().isEmpty(),// El middleware "Check", nos permite Validar si un campo, ya declarado en el modelo, cimple con la validación declarada ene l mismo middleware en este caso .not().isEmpty()
    check('email','El email es obligatorio').isEmail(),
    check('password','El password es obligatorio').not().isEmpty(),
    validarCampos
    ],
    postUsuario
)

router.get(`/`,
    [
        validarToken
    ],
    getUsuarios 
)


router.put('/:id',
    [
        validarToken,
        check('nombre','El nombre es obligatorio').not().isEmpty(),// El middleware "Check", nos permite Validar si un campo, ya declarado en el modelo, cimple con la validación declarada ene l mismo middleware en este caso .not().isEmpty()
        check('email','El email es obligatorio').isEmail(),
        validarCampos,

    ],

    putUsuario
)

router.delete('/:id',validarToken,deleteUsuario)






module.exports = router