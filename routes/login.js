/**
 * path: api/login
 */

const {Router} =  require('express');
const { check } = require('express-validator');

const {validarCampos} = require('../middlewares/validarCampos');

const {login, loginGoogle, renewToken} = require('../controllers/auth');
const { validarToken } = require('../middlewares/validarToken');



const router = Router();






router.post('/',
[
    check('email','El email es obligatorio').isEmail(),
    check('password','El password es obligatorio').not().isEmpty(),
    validarCampos

]
,login);

router.post('/google',
[
    check('token','El token de google es obligatorio').not().isEmpty(),
    validarCampos

]
,loginGoogle);


router.get('/renew',validarToken,renewToken);







module.exports = router
