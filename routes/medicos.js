const {Router} = require('express');
const router = Router();

const { check } = require('express-validator');

const { getMedicos, postMedico,putMedico, deleteMedico } = require('../controllers/medicos');

const { validarCampos } = require('../middlewares/validarCampos');
const { validarToken } = require('../middlewares/validarToken');




router.post(
    `/`,
    [
        validarToken,
        check('nombre','El nombre es obligatorio').not().isEmpty(),// El middleware "Check", nos permite Validar si un campo, ya declarado en el modelo, cimple con la validación declarada ene l mismo middleware en este caso .not().isEmpty()
        check('hospital','El id del hospital debe ser válido').isMongoId(),
        validarCampos
    ],
    postMedico
)

router.get(`/`,
    [
        validarToken
    ],
    getMedicos 
)


router.put('/:id',
    [
        validarToken,
        check('nombre','El nombre es obligatorio').not().isEmpty(),// El middleware "Check", nos permite Validar si un campo, ya declarado en el modelo, cimple con la validación declarada ene l mismo middleware en este caso .not().isEmpty()
        validarCampos,

    ],

    putMedico
)

router.delete('/:id',
    [
        validarToken
    ],
    deleteMedico
)


module.exports = router