
const {validationResult} = require('express-validator');

const validarCampos = (req, res, next)=>{
      /**
     * Verificamos los posibles erroes generados en los moddlewares de la ruta
     * 
     * 
     */

      console.log("VALIDANDO CAMPOS...");

      const errors = validationResult(req);

      if (!errors.isEmpty()) {

        console.log(errors);
         return res
         .status(400)
         .json({
             ok:false,
             errors:errors.mapped()
 
         })
         
 
         
      }

      next()
     

}

module.exports = {
    validarCampos
}