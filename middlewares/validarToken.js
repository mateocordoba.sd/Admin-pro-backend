const {response} = require('express');
const jwt = require('jsonwebtoken');

const validarToken = async (req, res = response, next) => {

    

    const token = req.header('jwt');


    if(!token){

        return res
         .status(400)
         .json({
             ok:false,
             errors:"No se envió token en la petición"
 
         })

    }


    try {



        const {uid} = jwt.verify(token,process.env.JWT_SECRET);

        req.uid = uid;

        console.log({uid});
        
    } catch (error) {

        return res
         .status(401)
         .json({
             ok:false,
             errors:"Token no válido"
 
         })
        
    }









    next()

}


module.exports = {
    validarToken
}